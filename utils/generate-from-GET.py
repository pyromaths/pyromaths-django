#!/usr/bin/env python3

# Copyright 2018 Louis Paternault
#
# Pyromaths is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pyromaths is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Pyromaths.  If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import sys
import textwrap
import urllib.parse

from pyromaths.ex import ExerciseBag
from pyromaths.outils.System import Fiche

# Un peu cracra, mais ce script ne sera jamais distribué (outil de développement uniquement).
sys.path.append(os.path.realpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir)))
from utiliser.views import parseSeeds

# Logging configuration
logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger()

BAG = ExerciseBag()

################################################################################
# Couleurs

GREEN = '\033[92m'
RED = '\033[91m'
ENDC = '\033[0m'

def green(text):
    return GREEN + text + ENDC

def red(text):
    return RED + text + ENDC
################################################################################

def usage():
    print(textwrap.dedent("""\
        Ce programme permet de trouver les exercices ayant produit des erreurs sur le serveur de pyromaths.

        USAGE:

        utils/generate-from-GET.py URL

        EXEMPLE:

        ./utils/generate-from-GET.py "http://example.com/creer/?EtatStableSysteme2=56&InterpolationMatrices=8%2C14%2C89"

        DESCRIPTION

        Ce programme teste chacun des exercices (avec graine) séparément, et affiche la liste des exercices ayant échoué.
        """))

def parse_arguments():
    if "-h" in sys.argv or "--help" in sys.argv:
        usage()
        sys.exit(0)

    if len(sys.argv) != 2:
        logging.error("Ce programme nécessite exactement un argument. Utilisez l'option `--help` pour afficher l'aide.")
        sys.exit(1)

    for key, value in urllib.parse.parse_qs(urllib.parse.unquote(urllib.parse.urlparse(sys.argv[1]).query)).items():
        if key not in BAG:
            continue
        for seed in parseSeeds(*value):
            yield key, seed

def compileexo(name, seed):
    parametres = {
        'enonce': True,
        'corrige': True,
        'exercices': [BAG[name](seed)],
        'titre': "Fiche de révisions",
        }
    try:
        with Fiche(parametres) as fiche:
            fiche.write_pdf()
    except:
        return False
    return True

if __name__ == "__main__":
    failed = []

    logging.info("Analyse des arguments")
    exercices = list(parse_arguments())

    logging.info("Test des exercices")
    for name, seed in exercices:
        if compileexo(name, seed):
            print(green("Succès"), end=" ")
        else:
            print(red("Échec "), end=" ")
            failed.append((name, seed))
        print("{}:{}".format(name, seed))

    logging.info("Bilan : Les erreurs suivantes ont été détectées")
    for name, seed in failed:
        print("pyromaths generate {}:{}".format(name, seed))
